<?php
/**
 * Created by PhpStorm.
 * User: 86130
 * Date: 2023/2/11
 * Time: 20:15
 */
$a="Bill & 'Steve'";

//echo  strpos($a,"world");
//echo  strrpos($a,"world");

//echo  $a;
//echo  htmlspecialchars($a);

//echo  htmlentities($a);


$textArr = [
    'Name' => 'textname',
    'Data' => [
        'title' => 'testtitle',
        'date'  => '20181015',
        'list'  => [
            'id1' => '123',
            'Di2' => '321',
        ]
    ],
    'page' => '3',
];

//多级数组拼接key值
$firstkeys = array_keys($textArr);//获取一级key

function rloop($array)
{
    global $firstkeys;
    global $full_key;
    $list = [];
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $full_key .= $key . '.';
            $list     = array_merge($list, rloop($value));
        } else {
            if (in_array($key, $firstkeys)) {  //说明是一级key
                $filename = strtolower($key);//一级key不需要拼接过长
            } else {
                $filename = strtolower($full_key . $key);
            }
            $list[$filename] = $value;
        }

    }
    unset($firstkeys);
    unset($full_key);
    return $list;
}

$arr=rloop($textArr);

ksort($arr);


$str="";

foreach($arr as $k=>$v){
    $str.=$k."=".$v.";";
}

//echo $str;


function fn($n ,$m)

{

    $arr = range(1,$n);

    $i = 0 ;    //设置数组指针

    while(count($arr)>1)

    {
        $i++;

        if($i % $m ==0) {

            unset($arr[$i]);

        }else{

            array_push($arr ,$arr[$i]); //本轮非出局猴子放数组尾部

            unset($arr[$i]);

        }

    }

    return $arr;

}
//print_r(fn(3,3));


$data=array(
    array(
        'id'=>1,
        'firstname'=>'Bill',
        'lastname'=>'Cxy',
    ),
    array(
        'id'=>2,
        'firstname'=>'Steve',
        'lastname'=>'Axy',
    ),
    array(
        'id'=>3,
        'firstname'=>'Mark',
        'lastname'=>'Zxy',
    ),
);

function array_sort($arr, $keys, $order=0) {
    if (!is_array($arr)) {
        return false;
    }
    $keysvalue = array();
    foreach($arr as $key => $val) {
        $keysvalue[$key] = $val[$keys];
    }
    if($order == 0){//升序 保留键值
        asort($keysvalue);
    }else {
        arsort($keysvalue);
    }
    reset($keysvalue);
    foreach($keysvalue as $key => $vals) {
        $keysort[$key] = $key;
    }
    $new_array = array();
    foreach($keysort as $key => $val) {
        $new_array[$key] = $arr[$val];
    }
    return $new_array;
}

$arr  =  array_sort($data,'lastname');

echo "<pre>";
var_dump($arr);
echo "</pre>";